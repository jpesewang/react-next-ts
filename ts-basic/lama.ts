let userObj: {
  name: string;
  age: number;
};

//FUNCTIONS
let sayHi = () => {
  console.log("Hi, welcome");
};

let funcReturnString = (): string => {
  return "this is a string";
};
let multiple = (num: number) => {
  return num * 2;
};
let sum = (a: number, b: number) => {
  return a + b;
};
